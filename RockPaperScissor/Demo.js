let userScore=0;
let compScore=0;
const msg=document.querySelector("#msg");
const choices=document.querySelectorAll(".choice");
const uScore=document.querySelector("#user-score");
const cScore=document.querySelector("#comp-score");
const generateOption=()=>{
    const opt=["rock","paper","secissor"];
    //math is a class in which random generate number betn 0 to 1 , we want 0 to 2 so *3
   const randInd= Math.floor(Math.random()*3);
   return opt[randInd];
}
const draw=()=>{
    console.log("Draw");
    msg.innerText="Draw!!";
    msg.style.backgroundColor="#081b31";

}
const showWinner=(user)=>{
    if(user==true){
        console.log("User win");
        userScore++;
        msg.innerText="You win!!";
        msg.style.backgroundColor="Green";
    }
    else{
        console.log("Computer win");
        compScore++;
        msg.innerText="You loose!!";
        msg.style.backgroundColor="red";
    }
    uScore.innerText=userScore;
    cScore.innerText=compScore;

}
const playGame=(userChoice)=>{
    //generate computer choice
    const compChoice=generateOption();
    console.log("User=",userChoice,"Computer=",compChoice);
    if(userChoice==compChoice){
        draw();
    }else{
        let userWin=true;
        if(userChoice==="paper"){
            userWin= compChoice==="secissor" ? false:true;
        }else if(userChoice==="rock"){
            userWin= compChoice==="paper" ? false:true;
        }else{
            userWin= compChoice==="rock"? false:true;
        }
        showWinner(userWin);
    }
    

}
choices.forEach((c)=>{
    c.addEventListener('click',()=>{
        const userChoice=c.getAttribute("id");
        playGame(userChoice);
    })
})
